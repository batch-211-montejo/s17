// showSampleAlert(); - will still work!
printName();
console.log("Hello World");

// Functions
	// Functions in javascript are lines/blocks of codes that tell our device/application to perform a ertain task when called/invoke
	// Functions are mostly created to create complicated tasks to run reveal several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform tha same task/function

	// Function declations
		// (function statements) defines a function with the specified parameters
		/*
			Syntax:

			function functionName() {
				code block(statement)
			}
		*/

		// function keywork - ised to define a JavaSxript function
		// functionName - the function name. Functions are named to be able to use later in the code
		// function block ({}) - the statements which comprise the body of the function. This is where the code is to be executed
		// we can assign a variable to hold a function, but that will be explained later

		function printName () {
			console.log("My name is John.");
		}

		printName();

		// decalredFunction ()


	// Function Decalrations vs Expression

	// Function declarations
	// a functo

		declaredFunction(); // declared functions can be hoisted, as long as the cinction has been defined
		function declaredFunction () {
			console.log ("Hello World from decalredFunction()")
		}

		// NOte: Hoisting is JS's behavior for certain variables and functions to run or use them before their declaration

		declaredFunction();

		// Functiion Expression
			// a function can also be stored in a variable. This is called a function expression
			// a function expression is an anonymous function assigned to the variableFunction

			// Anonymous function - function without a name

				// variableFunction(); error

			/*
				error - function expressions, being stored in a let or const variable cannot be hoisted			
			*/

			let variableFunction = function() {
				console.log("Hello Again");
			}

			variableFunction();

			// we can also create a function expression of a named function
			// However, to invoke the function expression, we invoke it by its variable name, not by its function name
			// Function expressions are always invoked (called) using the variable name


			let funcExpression = function funcName () {
				console.log("Hello From the other side");
			}

			// funcName(); - error
			funcExpression();


			// you can reassign decalred functions and function expressions to new anonyous functions
			declaredFunction = function() {
				console.log("updated decalredFunction");
			}

			declaredFunction();

			funcExpression = function () {
				console.log("updated funcExpression");
			}

			funcExpression();


			// however, we cannot reassign a function expression initialize with const
			const constantFunc = function() {
				console.log("initialized with const!");
			}
			constantFunc();

			// constantFunc = function () {
			// 	console.log("cannot be reassigned");
			// }
			// constantFunc();


		// Function scoping
		/*
			Scope is the accessibility (visibility) of variables within our program
			-JS Variables has 3 types of scope:
			1/ local/block scope
			2. global scope
			3. function scop
		*/
		// let globalvar = "Mr. WorldWide";
		// console.log(globalvar);

		{
			let localVar = "Armando Perez";
			console.log(localVar);
			// console.log(globalvar);//error
			// we cannot invkoe a global var inside a block if it is not decalred before our code block
		}

		let globalvar = "Mr. WorldWide";
		console.log(globalvar);
		// // console.log(localVar); // error
		// localVar being in a block, cannot be accessed outside of its code block

		// Function Scope
		/*
			JS has a function scope: Each function creates a new scope
			Variables defined inside a function are not accessible (visible) from outside the function
			Variables declared with var, let and const are quite similar when declared inside a function
		*/


		function showNames () {
			// function scoped variables
			var functionVar = "Joe";
			const functionConst = "John";
			let functionLet = "Jane";

			console.log(functionVar);;
			console.log(functionConst);
			console.log (functionLet);
		}
			showNames();
			// console.log(functionVar)
			// console.log(functionConst);
			// console.log (functionLet);

			/*
				the variables , functionvar, functionConst and functionLet are function scoped and cannot be accessesd outside of the function they were decalred in
			*/



		// Nested Functins

		// you can creaet another function inside a function
		// this is called nested function

		function myNewFunction () {
			let name = "Cee";

			function nestedFunction() {
				let nestedName = "Thor";
				console.log(name);
				console.log(nestedName)
			}
			// console.log(nestedName);
			// results to an error 
			// nestedName is not defined
			// nestedName variable, being decalred in the nestedFunction cannot be accessed outside of the function it was declared in
			nestedFunction();
		}

		myNewFunction();
		// nestedFunction();
		/*
			since this function is decalred inside myNew Function, ti too cannot be invoked outside of the function it was decalred in
		*/



		// Function and Global Scoped Variables

		// Global scoped variable
		let globalName = "Nej";

		function myNewFunction2() {
			let nameInside = "Martin";
			// variables decalred globally (outside any function) have Global scope
			// Global Variables can be accessed from anywhere in a JS program including from inside a function
			console.log(globalName);
		}

		myNewFunction2();


// Using alert
	// alert allows us to show a small window at the top of our browser page to show information to our users
	// as oppose to a console.log() which will only show the message on the console
	// it allows us to show a short dialog or instrucion to our user
	// the page will wait until the user dismisses the dialog


	// alert("hello world"); this will run immediately when the page loads

	// alert () syntax
	// alert("messaheInString");
	// you can do it numbers too

	// you can also use an alert to show a message to the user from a later function invocation

	function showSampleAlert() {
		alert("Hello, user!");
	}
	showSampleAlert();

	// you will fine that the page waits foe the user to dismiss the dialog before proceeding
	// you can witness this by reloading the page while the console is open
	console.log("i will only log in the console when the alert is dismissed");

	// notes on the use of alerts();
		// show only an alert() for short dialogs/messages to the user
		// do not overuse alert() because the program/js has to wait for it to be dismissed befoe continuing


	// using prompt ()

		// prompt() allows us to show a small window at the top of our browser to gather user inut
		// it much like alert (), wil have the page wait until the user complates or enters their input
		// the input from the prompt() will be returned as a string once the user dismisses the window

		let samplePrompt = prompt("Enter you Name.")
		console.log("Hello " + samplePrompt);

		/*
			prompt() syntax;

			prompt("<dialogString");
		*/

		let sampleNullPrompt = prompt("Don't enter anything");
		console.log(sampleNullPrompt);
		// prompt() returns an empty string when there is no input
		// null if the user cancels the prompt()

		// prompt() can be used for us to gather user input and be used in our code
		// however, since prompt() windows will have the page wait until the user dismisses the window it must not be oversued

		// prompt() used globally will be run immediately, so , for better user experience, it is much better to use them accordingly or add them in a function

		function printWelcomeMessage() {
			let firstName = prompt("Enter your First Name");
			let lastName= prompt("Enter your last Name");
			console.log("Hello " + firstName + " " + lastName + "!");
			console.log("Welcome to my page");
		}

		printWelcomeMessage();

		// Function Naming COnventions

		// function names should be definitive of the task it will perform. Ut usually contains a verb

		function getCourses() {
			let courses = ["Science 101", "Math 101", "English 101"];
			console.log(courses);
		}

		getCourses();

		// avoid heneric names to avoid confusion withing your code

		function get () {
			let name = "Jamie";
			console.log(name);
		}

		get();

		// avoid pointless and inappropriate function names

		function pikachu () {
			console.log(25%5);
		}
		pikachu();

		// Name your functions in small caps. Follow cameCase when naming variables and functions

		function displayCarInfo() {
			console.log("brand: toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");
		}
		displayCarInfo();
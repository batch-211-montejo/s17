/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
		function displayInfo () {
			let fullName = prompt("Enter your Full Name");
			let age= prompt("Enter your Age");
			let location = prompt("Enter your location");
			console.log("Hi, " + fullName);
			console.log("You are " + age + " years old");
			console.log("You are located in " + location);
			alert("Thank you for your input!");
		}
		displayInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//second function here:
	function faveBands(){
		let band1 = "Join The Club";
		let band2 = "Eraserhead";
		let band3 = "Prokya ni Edgar";
		let band4 = "Rivermaya";
		let band5 = "Kamikazee";
		console.log("1. " + band1);
		console.log("2. " + band2);
		console.log("3. " + band3);
		console.log("4. " + band4);
		console.log("5. " + band5);
	}
	faveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function faveMovies() {
		let movie1 = "Lou";
		let movie2 = "The Mist";
		let movie3 = "Incantation";
		let movie4 = "Luck";
		let movie5 = "Samaritan";
		console.log("1. " + movie1);
		console.log("Rotten Tomatoes Rating: 69%");
		console.log("2. " + movie2);
		console.log("Rotten Tomatoes Rating: 72%");
		console.log("3. " + movie3);
		console.log("Rotten Tomatoes Rating: 63%");
		console.log("4. " + movie4);
		console.log("Rotten Tomatoes Rating: 49%");
		console.log("5. " + movie5);
		console.log("Rotten Tomatoes Rating: 39%");
	}
	faveMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
// let printFriends() = function printUsers(){
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	// let friend1 = alert("Enter your first friend's name:"); 
	let friend1 = prompt("Enter your first friend's name:")
	// let friend2 = prom("Enter your second friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:");
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	// console.log(friends); 
	console.log(friend3); 
};


// console.log(friend1);
// console.log(friend2);
printFriends();
